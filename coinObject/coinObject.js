const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random() * 2)

        // 1. Um ponto: Randomicamente configura a propriedade “estado” do 
        // seu objeto moeda para ser um dos seguintes valores:
        // 0 ou 1: use "this.state" para acessar a propriedade "state" neste objeto.
    },
    toString: function() {
        
        if (this.state === 0){
            return "Heads"    
        }else if(this.state === 1){
            return "Tails"
        }
        // 2. Um ponto: Retorna a string "Heads" ou "Tails", dependendo de como
        //  "this.state" está como 0 ou 1.
    },
    toHTML: function() {

        const image = document.getElementById('img');
        image.classList.add("animation")
        if (this.state === 0){
            image.classList.remove("Tails")
            image.classList.add("Head")    
        }else if(this.state === 1){
            image.classList.remove("Head") 
            image.classList.add("Tails")
        }
        // 3. Um ponto: Configura as propriedades do elemento imagem 
        // para mostrar a face voltada para cima ou para baixo dependendo
        // do valor de this.state está 0 ou 1.
        
    }
 };
 
 function display20Flips() {
    const results = [];
    const pai = document.body
    const string = document.createElement("div")
    string.classList.add("string")
    string.classList.add("animation")
    string.id = "string"
    pai.appendChild(string)
    let counter = 0
    const timer = setInterval(function(){
                
                
                coin.flip()
                string.innerText = coin.toString()
                results.push(coin.toString())
                counter++
                if(counter === 20){
                    clearInterval(timer)
                }
                
        }, 1000)
    
    return results
    // 4. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma string na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
 
 }
 
 function display20Images() {
    const pai = document.body
    const results = [];
    let counter = 0
    const img = document.createElement("img")
    img.id = "img"
    img.className = "img"
    pai.appendChild(img)
    const timer = setInterval(function(){
            
        coin.flip()
        coin.toHTML()
        results.push(coin.toString())
        counter++
        if(counter === 20){
            clearInterval(timer)
        }
        
    }, 1000)
    return results       
    // 5. Um ponto: Use um loop para arremessar a moeda 20 vezes, cada vez 
    // mostrando o resultado como uma imagem na página. 
    // Depois de que seu loop terminar, retorne um array com o 
    // resultado de cada arremesso.
 
 }
